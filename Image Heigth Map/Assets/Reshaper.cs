﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reshaper : MonoBehaviour {

    public GameObject ramp;
    private List<GameObject> pixelObjects;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setupShaper()
    {
        GameObject[] pixelArray = GameObject.FindGameObjectsWithTag("Pixel");
        pixelObjects = new List<GameObject>();

        foreach (GameObject pxl in pixelArray)
        {
            pixelObjects.Add(pxl);
        }

        for (int i = 0; i < pixelObjects.Count; i++)
        {
            GameObject pixel = pixelObjects[i];
            if (pixel.transform.position.y < 1)
            {
                pixelObjects.Remove(pixel);
            } else
            {
                changeDiagonalNeighbours(pixel);
                //changeSingleObject(pixel);
            }
        }
    }

    private void changeDiagonalNeighbours(GameObject pixel)
    {
        if (Physics.CheckSphere(new Vector3(pixel.transform.position.x + 1, 1, pixel.transform.position.z + 1), 0.1f))
        {
            if(!Physics.CheckSphere(new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z + 1), 0.1f))
            {
                Instantiate(ramp, new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z + 1), Quaternion.identity);
            }
        } else if (Physics.CheckSphere(new Vector3(pixel.transform.position.x - 1, 1, pixel.transform.position.z + 1), 0.1f))
        {
            if (!Physics.CheckSphere(new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z + 1), 0.1f))
            {
                Instantiate(ramp, new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z + 1), Quaternion.Euler(0, 90, 0));
            }
        } else if (Physics.CheckSphere(new Vector3(pixel.transform.position.x + 1, 1, pixel.transform.position.z - 1), 0.1f))
        {
            if (!Physics.CheckSphere(new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z - 1), 0.1f))
            {
                Instantiate(ramp, new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z - 1), Quaternion.Euler(0, -90, 0));
            }
        } else if (Physics.CheckSphere(new Vector3(pixel.transform.position.x - 1, 1, pixel.transform.position.z - 1), 0.1f))
        {
            if (!Physics.CheckSphere(new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z - 1), 0.1f))
            {
                Instantiate(ramp, new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z - 1), Quaternion.Euler(0, 180, 0));
            }
        }
    }

    public void changeSingleObject(GameObject pixel)
    {
        if (Random.Range(0, 3) == 1 
            && !(Physics.CheckSphere(new Vector3(pixel.transform.position.x + 1, 1, pixel.transform.position.z + 1), 0.1f)
            && Physics.CheckSphere(new Vector3(pixel.transform.position.x + 1, 1, pixel.transform.position.z - 1), 0.1f) 
            && Physics.CheckSphere(new Vector3(pixel.transform.position.x - 1, 1, pixel.transform.position.z + 1), 0.1f)
            && Physics.CheckSphere(new Vector3(pixel.transform.position.x - 1, 1, pixel.transform.position.z - 1), 0.1f)
            && Physics.CheckSphere(new Vector3(pixel.transform.position.x + 1, 1, pixel.transform.position.z), 0.1f)
            && Physics.CheckSphere(new Vector3(pixel.transform.position.x - 1, 1, pixel.transform.position.z), 0.1f)
            && Physics.CheckSphere(new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z + 1), 0.1f)
            && Physics.CheckSphere(new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z - 1), 0.1f)))
        {
            Instantiate(ramp, new Vector3(pixel.transform.position.x, 1, pixel.transform.position.z), Quaternion.Euler(-90, 0, 180/*Random.Range(0, 180)*/));
            Destroy(pixel);
        }
    }
}
