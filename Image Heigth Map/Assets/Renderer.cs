﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Renderer : MonoBehaviour
{

    public Texture2D heigthMap;
    public float height = 1;
    public float colorDiff = 0;

    private Reshaper rsp;
    // Use this for initialization
    void Start()
    {
        rsp = GetComponent<Reshaper>();

        Color[] pixels = heigthMap.GetPixels(0, 0, heigthMap.width, heigthMap.height);
        List<GameObject> pixelObjects = new List<GameObject>();
        float lastGoY = 0;
        float thisGoY;

        for (int x = 0; x < heigthMap.width; x++)
        {
            for (int y = 0; y < heigthMap.height; y++)
            {
                Color color = pixels[(x * heigthMap.width) + y];
                GameObject pixel = GameObject.CreatePrimitive(PrimitiveType.Cube);
                pixel.transform.position = new Vector3(x, color.grayscale * height, y);
                pixel.GetComponent<MeshRenderer>().materials[0].color = color;
                //Debug.Log("Add Pixel: " + pixel.transform.position);
                pixel.tag = "Pixel";
                pixelObjects.Add(pixel);
            }
        }
        foreach (GameObject go in pixelObjects)
        {
            thisGoY = go.transform.position.y;

            if(lastGoY > 0)
            {
                if(Mathf.Abs(thisGoY - lastGoY) > colorDiff)
                {
                    go.transform.position = new Vector3(go.transform.position.x, 1, go.transform.position.z);
                } else
                {
                    go.transform.position = new Vector3(go.transform.position.x, 0, go.transform.position.z);
                }
            } else
            {
                go.transform.position = new Vector3(go.transform.position.x, 1, go.transform.position.z);
            }

            lastGoY = thisGoY;
        }

        rsp.setupShaper();
    }
}

